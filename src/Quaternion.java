import java.lang.reflect.Array;
import java.util.*;

/** Quaternions. Basic operations. */
public class Quaternion {

   private static final double eps = 0.000001;
   private double a;
   private double b;
   private double c;
   private double d;

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      this.a = a;
      this.b = b;
      this.c = c;
      this.d = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return this.a;
   }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() {
      return this.b;
   }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
      return this.c;
   }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
      return this.d;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      if (a < 0){ sb.append(this.a); }
      if (a >= 0){ sb.append(this.a); }
      if (b < 0){ sb.append(this.b).append("i"); }
      if (b >= 0){sb.append("+").append(this.b).append("i"); }
      if (c < 0){ sb.append(this.c).append("j"); }
      if (c >= 0){sb.append("+").append(this.c).append("j"); }
      if (d < 0){ sb.append(this.d).append("k"); }
      if (d >= 0){sb.append("+").append(this.d).append("k"); }

      return sb.toString();
   }

   /** Conversion from the string to the quaternion. 
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
      double[] numbers = new double[4];
      StringBuilder numb = new StringBuilder();
      int count = 0;
      int count2 = 0;
      int countI = 0;
      int countJ = 0;
      int countK = 0;
      int count3 = 0;
      String[] list = s.split("");
      int countIOrJ = 0;
      for (int i = 0; i < s.split("").length; i++){
         if (list[i].equals("i") || list[i].equals("j")){
            countIOrJ++;
            if (list[i+1].equals("i") || list[i+1].equals("j") || list[i+1].equals("k") || list[i+2].equals("i") || list[i+2].equals("j") || list[i+2].equals("k")){
               throw new IllegalArgumentException("Invalid syntax in " + s);
            }
         }
      }
      if (countIOrJ < 1){
         throw new IllegalArgumentException("Invalid syntax. Give all numbers " + s);
      }
      for (String str : s.split("")){
         if (count == 0 && str.equals("-")){
            numb.append(str);
         }
         else if (str.equals("i") || str.equals("j") || str.equals("k")){
            count3++;
            if (str.equals("i")){
               countI++;
            }
            if (str.equals("j")){
               countJ++;
            }
            if (str.equals("k")){
               countK++;
            }
            if (countI > 1 || countJ > 1 || countK > 1){
               throw new IllegalArgumentException("Double symbol " + str + " in " + s);
            }
            if (!(count3 == 1 && str.equals("i") || count3 == 2 && str.equals("j") || count3 == 3 && str.equals("k"))){
               throw new IllegalArgumentException("Invalid order in " + s);
            }
            if (count2 == 3){
               numbers[3] = Double.parseDouble(numb.toString());
               count2++;
            }
            continue;
         }
         else {
            try {
               Double.parseDouble(str);
               numb.append(str);
            } catch (NumberFormatException e) {
               if (str.equals(".")){
                  numb.append(".");
               }
               else if (str.equals("+") || str.equals("-")) {
                  if (count2 == 4){
                     throw new IllegalArgumentException("Too much arguments in expression " + s);
                  }
                  numbers[count2] = Double.parseDouble(numb.toString());
                  numb = new StringBuilder();
                  numb.append(str);
                  count2++;
               } else throw new IllegalArgumentException("Invalid symbol " + str + " in expression " + s);
            }
         }
         count++;
      }
      return new Quaternion(numbers[0], numbers[1], numbers[2], numbers[3]);
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(this.a, this.b, this.c, this.d);
   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      return a < eps && a > -eps && b < eps && b > -eps && c < eps && c > -eps && d < eps && d > -eps;
   }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      return new Quaternion(this.a, -this.b, -this.c, -this.d);
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      return new Quaternion(-this.a, -this.b, -this.c, -this.d);
   }

   /** Sum of quaternions. Expressed by the formula
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      return new Quaternion(this.a + q.a, this.b + q.b, this.c + q.c, this.d + q.d);
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      return new Quaternion(
              (this.a*q.a - this.b*q.b - this.c*q.c - this.d*q.d),
              (this.a*q.b + this.b*q.a + this.c*q.d - this.d*q.c),
              (this.a*q.c - this.b*q.d + this.c*q.a + this.d*q.b),
              (this.a*q.d + this.b*q.c - this.c*q.b + this.d*q.a)
      );
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      return new Quaternion(this.a * r, this.b * r, this.c * r, this.d * r);
}

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
      if (this.isZero()){
         throw new RuntimeException("Zero inverse in " + this);
      }
      return new Quaternion(
              (a/(a*a + b*b + c*c + d*d)),
              (-b/(a*a + b*b + c*c + d*d)),
              ((-c)/(a*a + b*b + c*c + d*d)),
              ((-d)/(a*a + b*b + c*c + d*d))
      );
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      return this.plus(q.opposite());
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      if (q.isZero()){
         throw new RuntimeException("Divide by Zero: " + q);
      }
      return this.times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
      if (q.isZero()){
         throw new RuntimeException("Divide by Zero: " + q);
      }
      return q.inverse().times(this);
   }
   
   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
      if (this == qo){return true;}
      if (qo instanceof Quaternion){
         Quaternion newQ = (Quaternion) qo;
         return this.minus(newQ).isZero();
      }
      return false;
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
      return (this.times(q.conjugate()).plus(q.times(this.conjugate()))).times(0.5);
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return this.toString().hashCode();
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(a*a+b*b+c*c+d*d);
   }

   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      System.out.println(valueOf("1-3j-2i-4k"));
   }
}

